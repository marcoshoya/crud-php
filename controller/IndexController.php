<?php

/**
 * IndexController
 */
class IndexController extends BaseController
{
    /**
     * Acao inicial
     */
    public function actionIndex()
    {
        $usuario = $this->getUsuario();
        
        if ($usuario) {
            $action = '/index/update/?id=' . $usuario->id;
        } else {
            $action = '/index/create';
        }
      
        $this->render('index', array(
          'usuario' => $usuario, 
          'action' => $action
        ));
    }
    
    /**
     * Ação inserir novo
     */
    public function actionCreate()
    {
        if ($_POST['Usuario']) {
            $post = $_POST['Usuario'];
            
            $usuario = new Usuario;
            $usuario->email = trim($post['email']);
            $usuario->password = trim($post['password']);
            $usuario->save();

            $this->redirect('/index/index/?id='.$usuario->id);
        }
    }
    
    /**
     * Ação remover
     */
    public function actionDelete()
    {
        $usuario = $this->getUsuario();
        $usuario->delete();
        
        $this->redirect('/index/index');
    }
    
    public function actionUpdate()
    {
        if ($_POST['Usuario']) {
            $post = $_POST['Usuario'];
            
            $usuario = $this->getUsuario();
            $usuario->email = trim($post['email']);
            $usuario->password = trim($post['password']);
            $usuario->update();

            $this->redirect('/index/index/?id='.$usuario->id);
        }
    }
    
    private function getUsuario()
    {
        $id = (int) filter_input(INPUT_GET, 'id');
        if ($id) {
            $usuario = UsuarioTable::find($id);
            
            if (!$usuario) {
                // redirect 404
                $this->redirect('/index/index');
            }
            
            return $usuario;
        }
        
        return null;
    }
}
