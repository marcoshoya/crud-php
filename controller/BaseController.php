<?php 

/**
 * BaseController
 *    Todos os controllers devem extender essa classe
 */
class BaseController
{
    /**
     * @var Connection
     */
    protected $em;
    
    /**
     * @var Twig
     */
    protected $twig;
    
    
    public function __construct($em, $twig)
    {
        $this->em = $em;
        $this->twig = $twig;
    }
    
    /**
     * Renderiza o template
     */
    public function render($name, $params = array())
    {
        $template = $this->twig->loadTemplate("$name.html.twig");
        echo $template->render($params);
    }
    
    public function redirect($url, $code = 302)
    {
        header("Location: {$url}", true, $code);
    }
}
