<?php 

/**
 * Classe UsuarioTable
 *  Implementa funções de repositorio
 */
class UsuarioTable
{
    /**
     * Busca pela PK
     */
    public static function find($id) 
    {
        $em = new Connection();

        $sql = "SELECT * FROM usuario WHERE id = " . (int) $id;
        
        try {
            
            $row = $em->query($sql);
            if ($row) {
                $usuario = new Usuario();
                $usuario->id = $row['id'];
                $usuario->email = $row['email'];
                $usuario->password = $row['password'];
              
                return $usuario;
            }
            
            return null;
            
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
    
    public static function save(Usuario $usuario)
    {
        $em = new Connection();
        $sql = "INSERT INTO usuario (email, password) VALUES ('{$usuario->email}', '{$usuario->password}')";
        
        try {
            
            $id = $em->execute($sql);
            
            $usuario->id = $id;
            
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
    
    public static function delete(Usuario $usuario)
    {
        $em = new Connection();
        $sql = "DELETE FROM usuario WHERE id = {$usuario->id}";
        
        try {
            
            $id = $em->execute($sql);
            
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
    
    public static function update(Usuario $usuario)
    {
        $em = new Connection();
        $sql = "UPDATE usuario SET email = '{$usuario->email}', password = '{$usuario->password}' 
                  WHERE id = {$usuario->id}";
        
        try {
            
            $id = $em->execute($sql);
            
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
}
