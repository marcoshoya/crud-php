<?php

class Usuario
{
    /**
     * @var int
     */
    public $id = null;
    
    /**
     * @var string
     */
    public $email;
    
    /**
     * @var string
     */
    public $password;
    
    
    public function __get($attr)
    {
        return $this->$attr;
    }
    
    public function __set($attr, $val)
    {
        $this->$attr = $val;
    }
    
    public function save()
    {
        UsuarioTable::save($this);
    }
    
    public function delete()
    {
        UsuarioTable::delete($this);
    }

    public function update()
    {
        UsuarioTable::update($this);
    }
}


