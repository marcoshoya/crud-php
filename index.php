<?php

  header('Content-Type: text/html; charset=utf-8');
  
  require_once 'config/app.php';

  try {
      $app = new App();
      $app->run();
      
  } catch (Exception $e) {
      die($e->getMessage());
  }
