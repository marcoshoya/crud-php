<?php

/**
 * Classe responsavel em gerenciar o acesso ao BD
 *
 * @since 2015-03-25
 */
class Connection
{    
    /**
     * @var resource conexao instanciada
     */
    private $conn = null;
    
    /**
     * Construtor da conexao
     */
    public function __construct()
    {
        if (null === $this->conn) {
          
            if (!class_exists('SQLite3')) {
              die('Extensão SQLite3 não instalada');
            }

            try {
                $this->conn = new SQLite3(__DIR__ . "/crud_bd", SQLITE3_OPEN_READWRITE);
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }
        
        return $this->conn;;
    }
    
    public function query($sql)
    { 
        return $this->conn->querySingle($sql, true);
    }
    
    public function execute($sql)
    {
        $this->conn->exec($sql);
        
        return $this->conn->lastInsertRowid();
    }
}
