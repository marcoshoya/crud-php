<?php

require_once 'vendor/twig/twig/lib/Twig/Autoloader.php';
require_once 'controller/BaseController.php';
require_once 'connection.php';

/**
 * App Class
 *  Classe responsavel por realizar o boot da aplicaçao
 *
 * @author Marcos Lazarin <marcoshoya at gmail dot com>
 */
class App
{
    const VIEW = 'view';
    
    private $twig;
    
    private $em;
    
    private $controller;
    
    private $action;
    
    private $defaultController = 'index';
    
    private $defaultAction = 'index';
    
    public function __construct()
    {
        $this->initTwig();
        $this->initDatabase();
        $this->initRoute();
        
        // autoload dos models
        spl_autoload_register(function ($class) {
            require_once 'model/' . $class . '.php';
        });
    }
    
    /**
     * Execut a aplicação
     */
    public function run()
    {
        $file = sprintf('controller/%sController.php', ucfirst($this->controller));
        
        // inclue e instancia o controlador
        if (file_exists($file)) {
            
            require_once $file;
            
            $controllerName = sprintf('%sController', ucfirst($this->controller));
            $actionName = sprintf('action%s', ucfirst($this->action));
            
            $instance = new $controllerName($this->em, $this->twig);
            
            if (!method_exists($controllerName, $actionName)) {
                die("Metodo {$actionName} não existe em {$controllerName}");
            }
            
            $instance->$actionName();
            
        } else {
            die("Arquivo {$file} não encontrado");
        }
    }
    
    /**
     * Inicia o twig template engine
     * 
     * @see http://twig.sensiolabs.org/doc/api.html
     * 
     * @return Twig_Environment
     */
    private function initTwig()
    {
        Twig_Autoloader::register();
      
        $loader = new Twig_Loader_Filesystem(self::VIEW);
        
        $this->twig = new Twig_Environment($loader);
    }
    
    /**
     * Inicia o BD
     */
    private function initDatabase()
    {
        $this->em = new Connection();
    }
    
    /**
     * Route
     *  vai obter os parametros da url para definir qual o 
     *  controller e metodo chamar
     * 
     *  O controller default é IndexController
     *  A action default é actionIndex
     */
    private function initRoute()
    {
        $params = explode('/', $_SERVER['REQUEST_URI']);
        
        $this->controller = isset($params[1]) && !empty($params[1]) ? $params[1] : $this->defaultController; 
        $this->action = isset($params[2]) && !empty($params[2]) ? $params[2] : $this->defaultAction;
    }
}
